package com.epam;

import com.epam.modal.BinaryTree;
import com.epam.modal.Tree;


public class Application {
    public static void main(String[] args) {
        Tree<Integer> tree = new BinaryTree();
        BinaryTree<Integer> e = (BinaryTree<Integer>) tree;
        Integer integer = new Integer(11);

        Integer integer1 = new Integer(10);
        tree.add(integer);
        tree.add(integer1);
        tree.add(integer1);
        tree.add(4);
        tree.add(6);
        tree.add(3);
        tree.add(2);
        tree.add(22);
        tree.add(17);
        tree.add(15);
        tree.add(16);
        tree.add(14);
        tree.add(23);
        tree.show();
        System.out.println(tree.remove(integer1));
        tree.show();

        tree.add(1);
        System.out.println();
        tree.show();
        System.out.println(tree.contains(1130));
    }
}
